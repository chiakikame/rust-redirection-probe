#![deny(missing_docs)]
/*!
A small library for checking if given link will redirect you to somewhere
by HTTP HEAD action.

```
use redirect_probe_lib::{Fetch, Redirection};

fn process_link(link: &str) {
    let mut fetch_engine = Fetch::new().expect("Error when creating fetcher");
    print!("Link {} ... ", link);
    let result = fetch_engine.fetch_url(&link);
    match result {
        Ok(redir) => {
            match redir {
                Redirection::NoRedirection => {
                    println!("=> does not redirect.");
                }
                Redirection::HttpRedirectTo(target) => {
                    println!("=> redirects (via 3XX) to {}", target);
                }
            }
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
}
```
*/

extern crate futures;
extern crate hyper;
extern crate hyper_tls;
extern crate tokio_core;
extern crate native_tls;

use std::fmt::{self, Display, Formatter};
use futures::future::{ok, result};
use futures::Future;
use hyper::{Client, Request, Response, Method, Uri};
use hyper::client::{HttpConnector, Connect};
use hyper_tls::HttpsConnector;
use hyper::header::Location;
use tokio_core::reactor::Core;
use std::error::Error;

///
/// Convenient function. Check if a link will redirect the user to somewhere
/// in synchronized mode.
///
pub fn fetch_url(url_string: &str) -> Result<Redirection, FetchError> {
    let mut fetch = Fetch::new()?;
    fetch.fetch_url(url_string)
}

///
/// Describing errors in fetching process
///
#[derive(Debug)]
pub enum FetchError {
    /// Error on parsing URI
    UrlError(String),
    /// Error from `hyper` crate
    HyperError(String),
    /// Error from IO system
    IOError(String),
    /// Error from `native-tls`
    TlsError(String),
}

///
/// Describing whether redirection will occur
///
pub enum Redirection {
    /// A redirection will occur by HTTP 3XX
    HttpRedirectTo(String),
    /// No redirection will take place
    NoRedirection,
}

///
/// Fetch & redirection check engine
///
/// Primary entry point of this library.
///
pub struct Fetch {
    client: Client<HttpConnector>,
    client_sec: Client<HttpsConnector<HttpConnector>>,
    core: Core,
}

/// General case of boxed [`Future`](trait.Future.html) object used in this crate
pub type FetchFutureL<'a, T> = Box<Future<Item = T, Error = FetchError> + 'a>;

/// Boxed [`Future`](trait.Future.html) object with `'static` lifetime.
pub type FetchFuture<T> = FetchFutureL<'static, T>;

impl Fetch {
    /// Create a new fetch & redirection analysis engine
    pub fn new() -> Result<Self, FetchError> {
        let core = Core::new()?;
        let client = Client::configure()
            .connector(HttpConnector::new(4, &core.handle()))
            .build(&core.handle());
        let client_sec = Client::configure()
            .connector(HttpsConnector::new(4, &core.handle())?)
            .build(&core.handle());
        Ok(Fetch {
            core,
            client_sec,
            client,
        })
    }

    /// Fetch a url in synchronized fashion
    pub fn fetch_url(&mut self, url_string: &str) -> Result<Redirection, FetchError> {
        let core = &mut self.core;
        let client = &self.client;
        let client_sec = &self.client_sec;
        let fr = if url_string.starts_with("https") {
            Fetch::_fetch_url_future(client_sec, url_string)
        } else {
            Fetch::_fetch_url_future(client, url_string)
        };
        core.run(fr)
    }

    /// Fetch a url. Will return a `Future` object
    pub fn fetch_url_future<'s>(&'s self, url_string: &str) -> FetchFutureL<'s, Redirection> {
        let client = &self.client;
        Fetch::_fetch_url_future(client, url_string)
    }

    fn _fetch_url_future<'s, T>(
        client: &'s Client<T>,
        url_string: &str,
    ) -> FetchFutureL<'s, Redirection>
    where
        T: Connect,
    {
        let ft = Box::new(
            Fetch::parse_uri(url_string)
                .and_then(move |uri| Fetch::make_request(client, uri))
                .and_then(|res| Fetch::process_response(res)),
        );
        ft
    }

    fn parse_uri(s: &str) -> FetchFuture<Uri> {
        Box::new(result(s.parse()).map_err(FetchError::from))
    }

    fn make_request<T>(client: &Client<T>, uri: Uri) -> FetchFuture<Response>
    where
        T: Connect,
    {
        let req = Request::new(Method::Head, uri);
        Box::new(client.request(req).map_err(FetchError::from))
    }

    fn process_response(rsp: Response) -> FetchFuture<Redirection> {
        let r = if rsp.status().is_redirection() {
            let location: Option<&Location> = rsp.headers().get::<Location>();
            match location {
                Some(loc) => ok(Redirection::HttpRedirectTo(to_string(loc))),
                None => ok(Redirection::NoRedirection),
            }
        } else {
            ok(Redirection::NoRedirection)
        };
        return Box::new(r);

        fn to_string(s: &str) -> String {
            s.to_owned()
        }
    }
}

impl From<std::io::Error> for FetchError {
    fn from(e: std::io::Error) -> Self {
        FetchError::IOError(e.description().to_owned())
    }
}

impl From<hyper::error::UriError> for FetchError {
    fn from(e: hyper::error::UriError) -> Self {
        FetchError::UrlError(e.description().to_owned())
    }
}

impl From<hyper::error::Error> for FetchError {
    fn from(e: hyper::error::Error) -> Self {
        FetchError::HyperError(e.description().to_owned())
    }
}

impl From<native_tls::Error> for FetchError {
    fn from(e: native_tls::Error) -> Self {
        FetchError::TlsError(e.description().to_owned())
    }
}

impl Error for FetchError {
    fn description(&self) -> &str {
        match self {
            &FetchError::HyperError(ref msg) => msg.as_ref(),
            &FetchError::UrlError(ref msg) => msg.as_ref(),
            &FetchError::IOError(ref msg) => msg.as_ref(),
            &FetchError::TlsError(ref msg) => msg.as_ref(),
        }
    }
}

impl Display for FetchError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            &FetchError::HyperError(ref msg) => write!(f, "Fetch error: Hyper error: {}", msg),
            &FetchError::UrlError(ref msg) => write!(f, "Fetch error: Hyper uri error: {}", msg),
            &FetchError::IOError(ref msg) => write!(f, "Fetch error: IO error: {}", msg),
            &FetchError::TlsError(ref msg) => write!(f, "Fetch error: Tls error: {}", msg),
        }
    }
}
