# Rust-redirect-probe tool

This is a tool for detecting whether a link will redirect you to somewhere.

## License

This application is licensed under GNU GPL v3

## Requirement

For running

* OpenSSL library

For building / development

* Rust programming language development environment
* Be able to build `native-tls` (i.e. need OpenSSL headers in Linux)

## Usage

To use it within terminal + arguments

```
$ ./redirect-probe-tui https://www.google.com https://twitter.com http://twitter.com
Link https://www.google.com ... => redirects (via 3XX) to https://www.google.com.tw/?gfe_rd=cr&dcr=0&ei=lDPmWeq5GvH88we8iZjICA
Link https://twitter.com ... => does not redirect.
Link http://twitter.com ... => redirects (via 3XX) to https://twitter.com/
$
```

To use it within terminal, and input links by `stdin` (which is suitable for using under Microsoft Windows)

```
$ ./redirect-probe-tui 
Entering stdin mode
https://www.google.com
Link https://www.google.com ... => redirects (via 3XX) to https://www.google.com.tw/?gfe_rd=cr&dcr=0&ei=1TPmWcDfK_D88welgp6gAQ
http://www.google.com
Link http://www.google.com ... => redirects (via 3XX) to http://www.google.com.tw/?gfe_rd=cr&dcr=0&ei=2zPmWcjWDfL88wfblZTQCw
http://twitter.com
Link http://twitter.com ... => redirects (via 3XX) to https://twitter.com/
http://facebook.org
Link http://facebook.org ... => redirects (via 3XX) to https://facebook.com/
```

## Limitation

Redirection via HTML meta tag, and via JavaScript are not detected by this program for now.