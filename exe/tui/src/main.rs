extern crate redirect_probe_lib;

use redirect_probe_lib::{Fetch, Redirection};
use std::env::args;

fn main() {
    let args: Vec<String> = args().skip(1).collect();

    if args.len() == 0 {
        use std::io::stdin;

        println!("Entering stdin mode");
        let mut buffer = String::new();

        while has_data(stdin().read_line(&mut buffer)) {
            process_link(&buffer.trim());
            buffer.clear()
        }
    } else {
        args.iter().for_each(|link| process_link(link));
    }
}

fn process_link(link: &str) {
    let mut fetch_engine = Fetch::new().expect("Error when creating fetcher");
    print!("Link {} ... ", link);
    let result = fetch_engine.fetch_url(&link);
    match result {
        Ok(redir) => {
            match redir {
                Redirection::NoRedirection => {
                    println!("=> does not redirect.");
                }
                Redirection::HttpRedirectTo(target) => {
                    println!("=> redirects (via 3XX) to {}", target);
                }
            }
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
}

fn has_data(result: Result<usize, std::io::Error>) -> bool {
    match result {
        Ok(n) => {
            n > 0
        },
        Err(_) => false
    }
}